# Maintainer: Ahmad Hasan Mubashshir <ahmubashshir@gmail.com>

_pkgname=anbox-launchers
pkgname=${_pkgname}-git
pkgver=r15
pkgrel=4
pkgdesc="Add Anbox App Launchers to Anbox Category in Desktop Menu."
arch=('any')
url="https://github.com/ahmubashshir/anbox-launchers"
license=('GPL')
groups=()
makedepends=('coreutils' 'git' 'imagemagick')
source=("git+https://github.com/ahmubashshir/${_pkgname}.git"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/android.png"
        "anbox-android.menu")
validpgpkeys=('916961EE198832DD70B628B356DB0538F60D951C')
sha256sums=('SKIP'
            '1da64522c124240c217414d4b9614292f84e673d6efee56c55cf1f60308895f0'
            'ed4374f312b480bbe97703a432372fd88548071881932a938f58b5310f4e712b'
            '39e6a346163134e70b18a1647ff9baa71dc6b6ccd18e29394ede0e05608d6579')

pkgver() {
    cd "${srcdir}/${_pkgname}"
    printf "r%s" "$(git rev-list --count HEAD)"
}

_anbox_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build () {
    cd "${srcdir}"
    echo -e "$_anbox_desktop" | tee com.${_pkgname}.desktop
}

package() {
    depends=('anbox' 'systemd' 'python-xdg')
    provides=('anbox-launchers')
    conflicts=('anbox-launchers')
    
    cd "${srcdir}/${_pkgname}"
    make DESTDIR="${pkgdir}" SYSCONFDIR=/etc install

    # Fix
    rm -rf "${pkgdir}/etc/xdg/menus/applications-merged/anbox-android.menu"
    install -Dm644 "${srcdir}/anbox-android.menu" "${pkgdir}/etc/xdg/menus/applications-merged/anbox-android.menu"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${_pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/com.${_pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/android.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${_pkgname}.png"
    done
}
